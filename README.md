# DevOps Task

Task for entering DevOps School

## What does the program do

Deploy containerized simple web calculator developed on ASP.NET Core on AWS.<br>
If the application code in gitlab repository changed, new docker container creates with applied changes on the same EC2 instance.

## How to run on my computer

Requirements:
- [Terraform](https://www.terraform.io/downloads)
- [AWS Account](https://aws.amazon.com/)

First, set up your AWS credentials: <br>
```bash
export AWS_ACCESS_KEY_ID={access key}
export AWS_SECRET_ACCESS_KEY={secret key}
```

By default, application runs on eu-central-1 in Frankfurt. <br>
You can change it in ./Infrastructure/main.tf:
```tf
provider "aws" { 
    region = "{region}"
}
```

Point your security key pair created for EC2 Instances:
```tf
resource "aws_instance" "dotnet_calculator" {
    # Ubuntu 20.04 LTS
    ami = "ami-0d527b8c289b4af7f"
    instance_type = "t3.micro"
    vpc_security_group_ids = [aws_security_group.web_connection.id]
    key_name = {your key's name}
    user_data = "${file("run_docker.sh")}"
}
```

In ./Infrastructure/ directory run commands:
```bash
terraform init
terraform plan (optional)
terraform apply
```

## Terraform code explanation

<h4>Instance creation</h4>

Creates AWS t3.micro instance with Ubuntu 20.04 on board. Sets up inbound and outbound connection permissions with AWS Security Group which is explained further. Point security key pair created for EC2 instances and sets user data which will be runned after the instance is created.
```tf
resource "aws_instance" "dotnet_calculator" {
    ami = "ami-0d527b8c289b4af7f"
    instance_type = "t3.micro"
    vpc_security_group_ids = [aws_security_group.web_connection.id]
    key_name = "dotnet_calculator_key"
    user_data = "${file("run_docker.sh")}"
}
```

Bash script set to user data is installing docker and running two containers. First is to run calcultor web application and the second is watchtower container: tracks changes of .NET application image, if the image was changed, removes active docker container and creates new with applied changes.
```bash
#!/bin/bash
sudo apt-get update && install curl
curl -sSL https://get.docker.com/ | sh
sudo docker run -d --name docker_site -p 80:80 registry.gitlab.com/sdy-1/devops-task
sudo docker run -d --name watchtower -v /var/run/docker.sock:/var/run/docker.sock containrrr/watchtower --cleanup -i 10
```

<h4>Elastic IP</h4>

Creates static IPv4 address so we can connect an instance with not changing its IP. 
```tf
resource "aws_eip" "ip" {
    instance = aws_instance.dotnet_calculator.id
    vpc      = true
}
```
<h4>Security group</h4>

Creates security group which control inbound and outbound traffic. Allows connections via HTTP, HTTPS and SSH protocols from any IPs and sets ALLOW ALL rule for egress.
```tf
resource "aws_security_group" "web_connection" {
    name = "Web connection"
    description = "Allow HTTP, HTTPS, SSH protocols"

    ingress {
        description = "HTTP"
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        description = "HTTPS"
        from_port = 443
        to_port = 443
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        description = "SSH"
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

    tags = {
        name = "web_connection"
    }
}
```

<h4>Alarm</h4>

Creates metrics and its alarms that control cpu utilization, ingress and egress network traffic.
```tf
resource "aws_cloudwatch_metric_alarm" "cpu_alarm" {
  alarm_name                = "cpu_alarm-1"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "2"
  metric_name               = "CPUUtilization"
  period                    = "120"
  statistic                 = "Average"
  namespace                 = "AWS/EC2"
  threshold                 = "30"
  alarm_description         = "Monitoring of instance cpu"
  dimensions = {
    InstanceId = aws_instance.dotnet_calculator.id
  } 
}

resource "aws_cloudwatch_metric_alarm" "network_in_alarm" {
  alarm_name                = "network_in_alarm-1"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "2"
  metric_name               = "NetworkIn"
  period                    = "120"
  statistic                 = "Average"
  namespace                 = "AWS/EC2"
  threshold                 = "9000000"
  alarm_description         = "Monitoring of instance inbound traffic"
  dimensions = {
    InstanceId = aws_instance.dotnet_calculator.id
  } 
}

resource "aws_cloudwatch_metric_alarm" "network_out_alarm" {
  alarm_name                = "network_out_alarm-1"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "2"
  metric_name               = "NetworkOut"
  period                    = "120"               
  statistic                 = "Average"
  namespace                 = "AWS/EC2"
  threshold                 = "175000"
  alarm_description         = "Monitoring of instance outbound traffic"
  dimensions = {
    InstanceId = aws_instance.dotnet_calculator.id
  } 
}
```

## Gitlab CI Pipeline

.gitlab-ci.yml configures CI for ASP.NET Core application and its image.<br>
It has 3 stages: 

1) Builds .NET application and its unit tests

```yml
dotnet_build:
  extends: .dotnet_image
  stage: build
  before_script:
    - dotnet restore
  script:
    - dotnet build
  tags:
    - dotnet
```
2) Runs unit test to validate application's work and saves a report

```yml
dotnet_unit-test:
  extends: .dotnet_image
  stage: test
  before_script:
    - dotnet restore
  script:
    - dotnet test
  artifacts:
    when: always
    reports:
      junit:
        report.xml
  tags:
    - dotnet
  only:
    - master
    - main
```


3) Creating a new docker image and pushing it to Gitlab Registry if operations before were succeed

```yml
docker_build:
  extends: .docker_image
  services:
    - docker:dind
  stage: prepare-image
  before_script:
    - cd ./SimpleCalculator.WebMvc/
    - docker login registry.gitlab.com -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD}
  script:
    - docker build -t ${CI_REGISTRY}/${CI_PROJECT_PATH}:latest .
    - docker push ${CI_REGISTRY}/${CI_PROJECT_PATH}:latest
  after_script:
    - docker logout ${CI_REGISTRY}
    - cd ../
  tags:
    - docker
  only:
    - master
    - main
```





## Try it out

You can find my site by its IP = 3.69.235.89<br>
or you can use http://www.boringq-devops-task.pp.ua/ domain that I set with Route53 AWS service
