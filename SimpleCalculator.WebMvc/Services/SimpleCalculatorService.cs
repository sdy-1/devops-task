﻿namespace SimpleCalculator.WebMvc.Services;

public class SimpleCalculatorService
{
    public static double Calculate(double left, double right, char sign) =>
        sign switch
        {
            '+' => Logic.SimpleCalculator.Add(left, right),
            '-' => Logic.SimpleCalculator.Subtract(left, right),
            '*' => Logic.SimpleCalculator.Multiply(left, right),
            '/' => Logic.SimpleCalculator.Divide(left, right),
            _ => throw new ArgumentException("There is no such operation", nameof(sign))
        };
}