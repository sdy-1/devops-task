﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using SimpleCalculator.WebMvc.Models;
using SimpleCalculator.WebMvc.Services;

namespace SimpleCalculator.WebMvc.Controllers;

public class HomeController : Controller
{
    [HttpGet]
    public IActionResult Index()
    {
        return View();
    }
    
    [HttpPost]
    public IActionResult Index(OperationModel model)
    {
        var sign = model.Sign switch
        {
            OperationSign.Add => '+',
            OperationSign.Subtract => '-',
            OperationSign.Multiply => '*',
            OperationSign.Divide => '/',
            _ => throw new ArgumentException("Wrong sign", nameof(model))
        };

        var result = 0d;
        try
        {
            result = SimpleCalculatorService.Calculate(model.FirstNumber, model.SecondNumber, sign);
            model.Result = result;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return RedirectToAction("Error");
        }

        return View(model);
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
    }
}