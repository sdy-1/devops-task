﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace SimpleCalculator.WebMvc.Models;

public enum OperationSign
{
    Add,
    Subtract,
    Multiply,
    Divide
}

public class OperationModel
{
    [Required]
    [Display(Name = "First Number")]
    public double FirstNumber { get; set; }

    [Required]
    [Display(Name = "Second Number")]
    public double SecondNumber { get; set; }

    [Required]
    [Range(0, 3)]
    [Display(Name = "Operation")]
    public OperationSign Sign { get; set; }
    
    [Display(Name = "Result")]
    public double Result { get; set; }
}