﻿namespace SimpleCalculator.WebMvc.Logic;

public static class SimpleCalculator
{
    public static double Add(double left, double right) => left + right;
    
    public static double Subtract(double left, double right) => left - right;
    
    public static double Multiply(double left, double right) => left * right;

    public static double Divide(double left, double right) =>
        right == 0
            ? throw new DivideByZeroException("Denominator is zero.")
            : left / right;
}