#!/bin/bash
sudo apt-get update && install curl
curl -sSL https://get.docker.com/ | sh
sudo docker run -d --name dotnet_calculator -p 80:80 registry.gitlab.com/sdy-1/devops-task
sudo docker run -d --name watchtower -v /var/run/docker.sock:/var/run/docker.sock containrrr/watchtower --cleanup -i 10