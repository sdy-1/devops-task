provider "aws" { 
    region = "eu-central-1"
}

resource "aws_instance" "dotnet_calculator" {
    # Ubuntu 20.04 LTS
    ami = "ami-0d527b8c289b4af7f"
    instance_type = "t3.micro"
    vpc_security_group_ids = [aws_security_group.web_connection.id]
    key_name = "dotnet_key"
    user_data = "${file("run_docker.sh")}"
}

resource "aws_eip" "ip" {
    instance = aws_instance.dotnet_calculator.id
    vpc      = true
}

resource "aws_security_group" "web_connection" {
    name = "Web connection"
    description = "Allow HTTP, HTTPS, SSH protocols"

    ingress {
        description = "HTTP"
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        description = "HTTPS"
        from_port = 443
        to_port = 443
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        description = "SSH"
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

    tags = {
        name = "web_connection"
    }
}

resource "aws_cloudwatch_metric_alarm" "cpu_alarm" {
  alarm_name                = "cpu_alarm-1"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "2"
  metric_name               = "CPUUtilization"
  period                    = "120"
  statistic                 = "Average"
  namespace                 = "AWS/EC2"
  threshold                 = "30"
  alarm_description         = "Monitoring of instance cpu"
  dimensions = {
    InstanceId = aws_instance.dotnet_calculator.id
  } 
}

resource "aws_cloudwatch_metric_alarm" "network_in_alarm" {
  alarm_name                = "network_in_alarm-1"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "2"
  metric_name               = "NetworkIn"
  period                    = "120"
  statistic                 = "Average"
  namespace                 = "AWS/EC2"
  threshold                 = "9000000"
  alarm_description         = "Monitoring of instance inbound traffic"
  dimensions = {
    InstanceId = aws_instance.dotnet_calculator.id
  } 
}

resource "aws_cloudwatch_metric_alarm" "network_out_alarm" {
  alarm_name                = "network_out_alarm-1"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "2"
  metric_name               = "NetworkOut"
  period                    = "120"               
  statistic                 = "Average"
  namespace                 = "AWS/EC2"
  threshold                 = "175000"
  alarm_description         = "Monitoring of instance outbound traffic"
  dimensions = {
    InstanceId = aws_instance.dotnet_calculator.id
  } 
}





