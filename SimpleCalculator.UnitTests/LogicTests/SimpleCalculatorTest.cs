﻿using System;
using System.Threading;
using Xunit;
using SimpleCalculator.WebMvc.Logic;

namespace SimpleCalculator.UnitTests.LogicTests;

public class SimpleCalculatorTest
{
    [Theory]
    [InlineData(3.4, 7.1, 10.5)]
    [InlineData(0, 0, 0)]
    [InlineData(-5, -5, -10)]
    public void Add_ShouldReturnCorrectResult(double left, double right, double expected)
    {
        var actual = WebMvc.Logic.SimpleCalculator.Add(left, right);
        Assert.Equal(expected, actual);
    }
    
    [Theory]
    [InlineData(10, 2, 8)]
    [InlineData(0, 0, 0)]
    [InlineData(-5, -5, 0)]
    public void Subtract_ShouldReturnCorrectResult(double left, double right, double expected)
    {
        var actual = WebMvc.Logic.SimpleCalculator.Subtract(left, right);
        Assert.Equal(expected, actual);
    }
    
    [Theory]
    [InlineData(10, 2, 20)]
    [InlineData(0, 0, 0)]
    [InlineData(-5, -5, 25)]
    public void Multiply_ShouldReturnCorrectResult(double left, double right, double expected)
    {
        var actual = WebMvc.Logic.SimpleCalculator.Multiply(left, right);
        Assert.Equal(expected, actual);
    }
    
    [Theory]
    [InlineData(10, 2, 5)]
    [InlineData(0, 1, 0)]
    [InlineData(-5, -5, 1)]
    public void Divide_ShouldReturnCorrectResult(double left, double right, double expected)
    {
        var actual = WebMvc.Logic.SimpleCalculator.Divide(left, right);
        Assert.Equal(expected, actual);
    }
    
    [Theory]
    [InlineData(10, 0)]
    [InlineData(0, 0)]
    [InlineData(-5, 0)]
    public void Divide_ShouldThrowDivideByZeroException(double left, double right)
    {
        Assert.Throws<DivideByZeroException>(() =>
            WebMvc.Logic.SimpleCalculator.Divide(left, right));
    }
}